@extends('layouts.app')

@section('pageTitle', 'About ·')
@section('title', 'About ·')
@section('description', 'Get things done socially with Taskord.')
@section('image', '')
@section('url', url()->current())

@section('content')
<div class="container-md">
    <div class="card">
        <div class="card-header pt-3 pb-3">
            <span class="h5">Heading</span>
            <div>Sub-heading</div>
        </div>
        <div class="card-body">
            Soon
        </div>
    </div>
</div>
@endsection
