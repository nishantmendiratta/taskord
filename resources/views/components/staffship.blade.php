@if ($background === 'dark')
<span class="badge-font border border-danger fw-bold ms-1 px-2 rounded-pill small text-white" title="Feature Release Label: Staff Ship">
    Staff Ship
</span>
@else
<span class="badge-font border border-danger fw-bold ms-1 px-2 rounded-pill small text-dark" title="Feature Release Label: Staff Ship">
    Staff Ship
</span>
@endif
