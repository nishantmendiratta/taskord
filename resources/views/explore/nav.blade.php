<ul class="nav nav-pills justify-content-center explore-nav bg-white py-3">
    <li class="nav-item">
        <a class="nav-link active" href="{{ route('explore') }}">Popular Tasks</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Makers</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Products</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Tasks</a>
    </li>
</ul>
