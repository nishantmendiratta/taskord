<button class="btn btn-success text-white float-md-end mb-3" wire:click="markAsRead" wire:loading.attr="disabled">
    <x-heroicon-o-check class="heroicon" />
    Mark all as Read
</button>
